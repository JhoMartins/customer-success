require 'minitest/autorun'
require 'timeout'

class CustomerSuccessBalancing
  def initialize(customer_success, customers, customer_success_away)
    @customer_success = customer_success
    @customers = customers
    @customer_success_away = customer_success_away
  end

  # Returns the id of the CustomerSuccess with the most customers
  def execute
    validate_inputs
    available_css = @customer_success.reject { |css| @customer_success_away.include?(css[:id]) }

    cutomer_distribution = distribute_customers(available_css)
    css_with_more_customers(cutomer_distribution)
  end

  private

  def validate_inputs
    m = @customers.length
    raise 'Invalid m' unless m >= 0 && m <= 1_000_000

    n = @customer_success.length
    raise 'Invalid n' unless n >= 0 && n <= 1_000

    t = @customer_success_away.length
    raise 'Invalid t' if t > (n / 2).floor

    validate_field(@customers, :id, 1_000_000)
    validate_field(@customer_success, :id, 1_000)
    validate_field(@customers, :score, 100_000)
    validate_field(@customer_success, :score, 10_000)
  end

  def validate_field(registers, field, max_value)
    registers.each do |register|
      raise "Invalid #{field}" unless register[field] >= 0 && register[field] <= max_value
    end
  end

  def distribute_customers(available_css)
    @customers.map do |customer|
      customer_success = delegate_customer_success(available_css, customer)
      next customer if customer_success.nil?

      customer.merge(customer_success: customer_success[:id])
    end
  end

  def delegate_customer_success(available_css, customer)
    possible_css = available_css.select do |css|
      css[:score] >= customer[:score]
    end

    possible_css.min_by { |css| css[:score] }
  end

  def css_with_more_customers(cutomer_distribution)
    count = cutomer_distribution.each_with_object(Hash.new(0)) do |item, hash|
      next if item[:customer_success].nil?

      hash[item[:customer_success]] += 1
    end
    return 0 if count.empty?

    values = count.values
    max_value = count.max_by(&:last)
    return 0 if values.count(max_value.last) > 1

    max_value.first
  end
end

class CustomerSuccessBalancingTests < Minitest::Test
  def test_scenario_one
    css = [{ id: 1, score: 60 }, { id: 2, score: 20 }, { id: 3, score: 95 }, { id: 4, score: 75 }]
    customers = [{ id: 1, score: 90 }, { id: 2, score: 20 }, { id: 3, score: 70 }, { id: 4, score: 40 }, { id: 5, score: 60 }, { id: 6, score: 10}]

    balancer = CustomerSuccessBalancing.new(css, customers, [2, 4])
    assert_equal 1, balancer.execute
  end

  def test_scenario_two
    css = array_to_map([11, 21, 31, 3, 4, 5])
    customers = array_to_map([10, 10, 10, 20, 20, 30, 30, 30, 20, 60])
    balancer = CustomerSuccessBalancing.new(css, customers, [])
    assert_equal 0, balancer.execute
  end

  def test_scenario_three
    customer_success = Array.new(1000, 0)
    customer_success[998] = 100

    customers = Array.new(10000, 10)

    balancer = CustomerSuccessBalancing.new(array_to_map(customer_success), array_to_map(customers), [1000])

    result = Timeout.timeout(1.0) { balancer.execute }
    assert_equal 999, result
  end

  def test_scenario_four
    balancer = CustomerSuccessBalancing.new(array_to_map([1, 2, 3, 4, 5, 6]), array_to_map([10, 10, 10, 20, 20, 30, 30, 30, 20, 60]), [])
    assert_equal 0, balancer.execute
  end

  def test_scenario_five
    balancer = CustomerSuccessBalancing.new(array_to_map([100, 2, 3, 3, 4, 5]), array_to_map([10, 10, 10, 20, 20, 30, 30, 30, 20, 60]), [])
    assert_equal balancer.execute, 1
  end

  def test_scenario_six
    balancer = CustomerSuccessBalancing.new(array_to_map([100, 99, 88, 3, 4, 5]), array_to_map([10, 10, 10, 20, 20, 30, 30, 30, 20, 60]), [1, 3, 2])
    assert_equal balancer.execute, 0
  end

  def test_scenario_seven
    balancer = CustomerSuccessBalancing.new(array_to_map([100, 99, 88, 3, 4, 5]), array_to_map([10, 10, 10, 20, 20, 30, 30, 30, 20, 60]), [4, 5, 6])
    assert_equal balancer.execute, 3
  end

  def array_to_map(arr)
    out = []
    arr.each_with_index { |score, index| out.push({ id: index + 1, score: score }) }
    out
  end
end

Minitest.run